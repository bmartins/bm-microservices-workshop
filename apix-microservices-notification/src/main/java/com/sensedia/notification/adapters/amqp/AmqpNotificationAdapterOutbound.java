package com.sensedia.notification.adapters.amqp;

import com.sensedia.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.notification.adapters.amqp.config.BrokerOutput;
import com.sensedia.notification.ports.AmqpPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import static com.sensedia.commons.headers.DefaultHeader.APP_ID_HEADER_NAME;
import static com.sensedia.commons.headers.DefaultHeader.EVENT_NAME_HEADER_HEADER;
import static com.sensedia.notification.adapters.amqp.config.EventConfig.NOTIFICATION_OPERATION_ERROR_EVENT_NAME;

@Slf4j
@Service
@EnableBinding({BrokerOutput.class})
public class AmqpNotificationAdapterOutbound implements AmqpPort {

    private final BrokerOutput output;

    @Value("${spring.application.name}")
    protected String appId;

    @Autowired
    public AmqpNotificationAdapterOutbound(BrokerOutput output) {
        this.output = output;
    }

    @Override
    public void notifyNotificationOperationError(DefaultErrorResponse errorResponse) {
        log.error("error in account creation");
        sendMessage(output.publishNotificationOperationError(), errorResponse, NOTIFICATION_OPERATION_ERROR_EVENT_NAME);
    }

    private void sendMessage(MessageChannel channel, Object object, String eventName) {
        channel.send(
                MessageBuilder.withPayload(object)
                        .setHeader(EVENT_NAME_HEADER_HEADER, eventName)
                        .setHeader(APP_ID_HEADER_NAME, appId)
                        .build());
    }
}
